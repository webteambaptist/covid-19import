﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NLog;
using COVID19LIImport.Models;
using System.Net;

namespace COVID19LIImport
{
    class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];

        private static readonly string LIUserName = ConfigurationManager.AppSettings["User"];
        private static readonly string LIPassword = ConfigurationManager.AppSettings["Pass"];
        private static readonly string LIDomain = ConfigurationManager.AppSettings["Domain"];
        private static readonly string ImportUser = ConfigurationManager.AppSettings["ImportUser"];
        private static readonly string ImportPassword = ConfigurationManager.AppSettings["ImportPassword"];
        private static readonly string SourceDirectory = ConfigurationManager.AppSettings["LeadingIncidatorFileIn"];
        private static readonly string OutDirectory = ConfigurationManager.AppSettings["LeadingIncidatorFileOut"];
        private static readonly string SourceFileName = ConfigurationManager.AppSettings["SourceFileName"];
        private static readonly string DestFileName = ConfigurationManager.AppSettings["DestFileName"];

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
            int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        private const string NewLine = "<br />";

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);

        private static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();

            var file = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\COVID19-LeadingIndicators-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, file);
            LogManager.Configuration = config;

            try
            {
                Logger.Info("Starting COIVD-19 Leading Indicator Service");
                RunProcess();

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message +
                               NewLine + " ex.InnerException: " + ex.InnerException + NewLine);
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message +
                           NewLine + " ex.InnerException: " + ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "COIVD-19 Leading Indicator Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                Console.WriteLine(ex.Message);
            }
        }

        private static void RunProcess()
        {
            try
            {
                Console.WriteLine("Starting COVID-19 Leading Indicators Import");
                Logger.Info("Starting COVID-19 Leading Indicators Import");

                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "COVID-19 Leading Indicator Process Started....",
                    Subject = "COVID-19 Leading Indicator Process Started...."
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                //GetFiles();
                //string sourceFilename = "COVID Leading Indicators.pdf";
            
                // Use Path class to manipulate file and directory paths.
                string sourceFile = Path.Combine(SourceDirectory, SourceFileName);
                //string destFileName = "COVID-19 Leading Indicators and Preparedness.pdf";
                string destFile = Path.Combine(OutDirectory, DestFileName);

                // if File exists delete it first
                if (File.Exists(destFile))
                {
                    // file exists so I will delete it
                    File.Delete(destFile);// then it should create a new one
                }

                // To copy a folder's contents to a new location:
                // Create a new target folder.
                // If the directory already exists, this method does not create a new directory.
                if (!Directory.Exists(OutDirectory))
                {
                    Directory.CreateDirectory(OutDirectory);
                }

                // To copy a file to another location and
                // overwrite the destination file if it already exists.
                File.Copy(sourceFile, destFile, true);

                m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "COVID-19 Leading Indicator Process Finished...",
                    Subject = "COVID-19 Leading Indicator Process Finished..."
                };

                res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }


                Console.WriteLine("COVID-19 Leading Indicator Import Complete!");
                Logger.Info("COVID-19 Leading Indicator Import Complete!");
                GC.Collect();
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " +
                               ex.Message + NewLine + " ex.InnerException: " + ex.InnerException + NewLine);
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " +
                           ex.Message + NewLine + " ex.InnerException: " + ex.InnerException + NewLine +
                           "Full Exception: " + ex,
                    Subject = "COVID-19 Leading Indicator Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                Console.WriteLine(ex.Message);
            }
        }
        //private static IEnumerable<LeadingIndicators> GetFiles()
        //{
        //    try
        //    {
        //        var copyDir = LIFiles;
        //        var leadingIndicators = new List<LeadingIndicators>();

        //        if (!Directory.Exists(copyDir))
        //        {
        //            Console.WriteLine("Creating directory, {0}", copyDir);
        //            Logger.Info("Creating directory, {0}", copyDir);
        //            Directory.CreateDirectory(copyDir);
        //        }

        //        try
        //        {
        //            try
        //            {
        //                const int logon32ProviderDefault = 0;

        //                const int logon32LogonInteractive = 2;
        //                var returnValue = LogonUser(LIUserName, LIDomain, LIPassword, logon32LogonInteractive,
        //                    logon32ProviderDefault, out var safeTokenHandle);
                        
        //                if (false == returnValue)
        //                {
        //                    var ret = Marshal.GetLastWin32Error();
        //                    throw new System.ComponentModel.Win32Exception(ret);
        //                }

        //                using (safeTokenHandle)
        //                {
        //                    using (var newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
        //                    {
        //                        using (var impersonatedUser = newId.Impersonate())
        //                        {
        //                            var files = Directory.GetFiles(LIDirectory);
        //                            foreach (var item in files)
        //                            {
        //                                var words = item.Split('\\');
        //                                var filename = words[words.Length - 1];
        //                                Console.WriteLine(filename);
        //                                Logger.Info(filename);
        //                                AddDocument(copyDir, filename, LIDirectory, words);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("Program/getFiles();  #region tryCatchRegion3" + ex.Message);
        //                var m = new Mail()
        //                {
        //                    From = From,
        //                    To = To,
        //                    Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion3 " +
        //                           NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
        //                           ex.InnerException + NewLine + "Full Exception: " + ex,
        //                    Subject = "COVID-19 Leading Indicators Process Failed"
        //                };

        //                var res = SendMail.SendMailMessage(m);
        //                if (res.StatusCode == HttpStatusCode.OK)
        //                {
        //                    Console.WriteLine("Email sent successfully!");
        //                    Logger.Info("Email sent successfully!");
        //                }
        //                else
        //                {
        //                    Console.WriteLine("Error sending Email");
        //                    Logger.Info("Error sending Email");
        //                }
        //                // SendMail.SendMail.SendMailService(m);
        //                Console.WriteLine(ex.Message);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Error("Program/getFiles();  #region tryCatchRegion2" + ex.Message);
        //            var m = new Mail()
        //            {
        //                From = From, 
        //                To = To,
        //                Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion2 " +
        //                       NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
        //                       ex.InnerException + NewLine + "Full Exception: " + ex,
        //                Subject = "COVID-19 Leading Indicators Process Failed"
        //            };

        //            var res = SendMail.SendMailMessage(m);
        //            if (res.StatusCode == HttpStatusCode.OK)
        //            {
        //                Console.WriteLine("Email sent successfully!");
        //                Logger.Info("Email sent successfully!");
        //            }
        //            else
        //            {
        //                Console.WriteLine("Error sending Email");
        //                Logger.Info("Error sending Email");
        //            }
                    
        //            Console.WriteLine(ex.Message);
        //        }

        //        return leadingIndicators;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Program/getFiles();  #region tryCatchRegion1" + ex.Message);
        //        var m = new Mail()
        //        {
        //            From = From, 
        //            To = To,
        //            Body = "Exception occurred while running Program.getFiles();  #region tryCatchRegion1 " + NewLine +
        //                   " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " + ex.InnerException +
        //                   NewLine + "Full Exception: " + ex,
        //            Subject = "COVID-19 Leading Indicators Process Failed"
        //        };

        //        var res = SendMail.SendMailMessage(m);
        //        if (res.StatusCode == HttpStatusCode.OK)
        //        {
        //            Console.WriteLine("Email sent successfully!");
        //            Logger.Info("Email sent successfully!");
        //        }
        //        else
        //        {
        //            Console.WriteLine("Error sending Email");
        //            Logger.Info("Error sending Email");
        //        }
        //        // SendMail.SendMail.SendMailService(m);
        //        Console.WriteLine(ex.Message);
        //        return null;
        //    }
        //}

        //private static void AddDocument(string copyDir, string filename, string lIDirectory, IReadOnlyList<string> words)
        //{
        //    try
        //    {
        //        byte[] bytes = File.ReadAllBytes(Path.GetFullPath(lIDirectory + "\\" + filename));
        //        writeBytesToFile(copyDir + "\\" + "COVID-19 Leading Indicators and Preparedness.pdf", bytes);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Program/addDocument()" + ex.Message);

        //        Console.WriteLine(ex.Message);
        //    }
        //}
        //private static void writeBytesToFile(String fileOutput, byte[] bytes)
        //{
        //    const int logon32ProviderDefault = 0;
        //    //This parameter causes LogonUser to create a primary token.
        //    const int logon32LogonInteractive = 2;
        //    var returnValue = LogonUser(ImportUser, LIDomain, ImportPassword, logon32LogonInteractive,
        //        logon32ProviderDefault, out var safeTokenHandle);
           
        //    if (false == returnValue)
        //    {
        //        var ret = Marshal.GetLastWin32Error();
        //        throw new System.ComponentModel.Win32Exception(ret);
        //    }

        //    using (safeTokenHandle)
        //    {
        //        using (var newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
        //        {
        //            using (var impersonatedUser = newId.Impersonate())
        //            {
        //                File.WriteAllBytes(fileOutput, bytes);
        //            }
        //        }
        //    }
        //}
        private sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            private SafeTokenHandle()
                : base(true)
            {
            }

            [DllImport("kernel32.dll")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            [SuppressUnmanagedCodeSecurity]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool CloseHandle(IntPtr handle);

            protected override bool ReleaseHandle()
            {
                return CloseHandle(handle);
            }
        }
    }
}
