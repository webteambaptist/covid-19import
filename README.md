**If file fails to copy to Sitecore

1. Move file from \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators to your local temp folder
2. Move the file back to \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators
3. Log into CMS https://ppcm.bh.local/sitecore and verify the file is updated in Media Library folder.
4. Publish the file.
5. Verify it's now updated in the live site
---

## if file fails to copy to the source directory that sitecore pulls from

##This is the source directory

1. Grab file from \\bhpappmka02v\Spotfire TransferNM
2. copy that file into your local temp folder and change the name to match whats in \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators
3. delete the file from \\bhpiiswfe07v\d$\SitecoreUpload\COVID-19\Leading Indicators and move the file from your local temp folder into the Sitecore Upload folder. Then just publish from sitecore.

---
